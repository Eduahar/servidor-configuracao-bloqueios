package br.com.itau.servidorconfiguracaobloqueios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ServidorConfiguracaoBloqueiosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServidorConfiguracaoBloqueiosApplication.class, args);
	}

}
